# TP 10 - Mise en situation

## Namespace
* name: tp-dev
* label: 
  * app: tp

## ConfigMap
* name: tp-cm
* Value :
  * TITLE: Coucou
  * MESSAGE: Hello Néosoft de l'environnement de dev

## Volume
### PVC
* name: tp-pvc
* storageClass: microk8s-hostpath
* request: 1Gi
* accessMode: ReadWriteMany

## Deployment
* name: tp-deploy
* volume: 
  * name: log
  * persistentVolumeClaim: tp-pvc
* container 1:
  * image: kguerin/nginx-tp
  * name: nginx
  * envFrom: tp-cm
  * volume: log
    * path: /var/log/nginx/
* container 2:
  * image: alpine:3.18
  * name: nginx-log
  * volume: log
    * path: /nginx-log
  * command : tail -f /nginx-log/access.log

## Service
* name: tp-svc
* type: clusterIP
* port: 80
* targetPort: 80

## Ingress
* name: tp-ing 
* host: tp.dev.${USER}.docker-k8s-training-202310.labneosoft.fr


# Nouvel environnement
Reproduire la même configuration sur un namespace nommé `tp-prod`
Pensez à modifier le contenu du configMap

