# TP3.5 - Docker Compose

Manipulation de docker compose


## Services
### phpmyadmin
- image: phpmyadmin:latest  
- port: 8080 vers 80
- environment: 
  - PMA_ARBITRARY=1
- networks: lan, net

### mysql
- image: mariadb:latest
- environment:
  - MYSQL_ROOT_PASSWORD: notSecureChangeMe
- networks: lan

## Networks
### lan
- name: lan
- internal network

### net
- name: net