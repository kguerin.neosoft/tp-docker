## Créer un Service

### Avec le mode impératif 

Avec la commande `kubectl expose`, créez votre premier service

```bash
kubectl expose deployment training-${USER}-deploy --port=80 --target-port=8080
```

Cherchez l'IP de ce service `kubectl get service` ou `kubectl describe service` puis effectuez un `curl` sur cette IP

```bash
kubectl describe service
curl http://<ip>
```

Vous devriez voir Hello World affiché, avec d'autres informations provenant du backend déployé !

> Vous pouvez maintenant le supprimer avec `kubectl delete`

### Avec le mode déclaratif

Créez un Service de type ClusterIP, de façon déclarative

Utilisez le fichier `5_service-cp.yaml` et la commande `kubectl create` pour créer votre service de type ClusterIP

```bash
kubectl create -f 5_service-cp.yaml
```

Interrogez maintenant votre service grâce à son IP, récupérée grâce à la commande `kubectl get svc`

```bash
kubectl get svc
curl http://<IP>
```
---

Nous allons maintenant créer un service de type NodePort, de façon déclarative

Utilisez le fichier `5_service-np.yaml` et la commande `kubectl create` pour créer votre service de type NodePort

```bash
kubectl create -f 5_service-np.yaml
```

Interrogez maintenant votre service avec l'url 127.0.0.1 et le port indiqué par la commande `kubectl get svc`

```bash
kubectl get svc
curl http://127.0.0.1:<port>
```

> Le port à cibler est un port aléatoire, libre sur votre instance, par exemple `31445/TCP` dans la colonne Port(s) contenant `80:31445/TCP`
