## Débrouille toi

### Etape 1
* namespace
  * nom: neosoft-training-$USER
* Deployment
  * nom: web
  * label: tier=front, app=web
  * replicas: 2
  * image: nginxdemos/hello:0.2
* Service
  * type: clusterIP
  * port: 8080
  * targetPort: 80
* Ingress
  * path: /
  * host: demo.${DNS_HOST}

> Tip : kubectl create ing test --rule=host/path=service:port

### Etape 2
Modifier le nombre de replicas à 4

### Etape 3
Change le nom de l'image en nginxdemos/hello:0.3
