## Créer un Deployment

### Avec le mode impératif 

Avec la commande `kubectl create`, créez votre premier deployment

```bash
kubectl create deployment test-${USER} --image=nginx
```

Cherchez l'IP du Pod créé par votre Deployment avec `kubectl describe pod` puis effectuez un `curl` sur cette IP

```bash
kubectl describe pod
curl http://<ip>
```

Vous devriez voir la page d'accueil NGinx !

> Vous pouvez maintenant le supprimer avec `kubectl delete`

### Avec le mode déclaratif

Utilisez le fichier `3_deployment.yaml` et la commande `kubectl create` pour créer votre 2nd deployment

```bash
kubectl create -f 3_deployment.yaml
```

### Modifier le nombre de replicas

Utilisez `kubectl scale` pour augmenter le nombre de replicas

> Conservez ce Deployment qui nous servira pour la suite
