## Créer un Pod

### Avec le mode impératif 

Avec la commande `kubectl run`, créez votre premier pod

```bash
kubectl run test-${USER} --image=nginx
```

Cherchez l'IP du Pod avec `kubectl describe pod test-${USER}` puis effectuez un `curl` sur cette IP

```bash
kubectl describe pod test-${USER}
curl http://<ip>
```

Vous devriez voir la page d'accueil NGinx !

> Vous pouvez maintenant le supprimer avec `kubectl delete`

### Avec le mode déclaratif

Utilisez le fichier `1_pod.yaml` et la commande `kubectl create` pour créer votre 2nd pod

Générer un manifest avecc cette commande :
```bash
kubectl run training-${USER} --image=nginx --dry-run=client -oyaml > 1_pod.yaml
```

Puis l'appliquer :

```bash
kubectl create -f 1_pod.yaml
```
